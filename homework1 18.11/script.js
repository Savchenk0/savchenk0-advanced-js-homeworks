class Employee {
    constructor(name,age,salary){
        this._age = age
        this._name = name
        this._salary = salary
    }
    set salary(value){
        this._salary = value
    }
    get salary(){
        return this._salary
    }
    set age(value){
        this._age = value
    }
    get age(){
        return this._age
    }
    set name(value){
        this._name = value
    }
    get name(){
        return this._name
    }
}

class Programmer extends Employee {
    constructor(name,age,salary,lang){
        super(name,age,salary)
        this._lang = lang
    }
    get salary(){
        return this._salary * 3
    }
}

const grigory = new Programmer('Grigory',23,2000,`french english russian`)
const eugene = new Programmer('Eugene',27,3000,[`korean `,`english`, `russian`])
console.log(eugene,grigory,eugene.salary)
