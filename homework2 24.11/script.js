const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
  ];
// get filtred array with objects of 3 required properties
function validateBooksArr(array){
    return  array.reduce((acc,el,index)=>{
       try{ if (el.author && el.name && el.price){
            acc.push(el)
        } 
      else throw new Error('some information remains unset')}
        catch(err){
          if (el.author == undefined){
            console.log(`author is not set at index number ${index}`)
          }
          if (el.name == undefined){
            console.log(`name is not set at index number ${index}`)
          }
          if (el.price == undefined){
            console.log(`price is not set at index number ${index}`)
          }
        }
        return acc
        },[])
}
// show list from the fnctn above at the specific element on the page
function showListOnEl(listArray,element){
      element.innerHTML = listArray.reduce((acc,el,index) => {
            let {author:_author,
                name:_name,
                price:_price} = el
                if (index === 0) acc+= `<ul>`
                acc += `<li> Автор: ${_author}<br> Название: ${_name} <br> Цена:${_price}</li>`
                if (index === listArray.length - 1) acc += `</ul>`
            return acc
        },'')   
        // return element.innerHTML
}
// set our element
let listWrapper = document.getElementById('root')
// combine both functions
function getNShowList(array,element){
    return showListOnEl(validateBooksArr(array),element)
}
// function call
getNShowList(books,listWrapper)